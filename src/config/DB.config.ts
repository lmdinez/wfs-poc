import { registerAs } from '@nestjs/config';
import { User } from 'src/auth/user.entity';


export default registerAs('mysqlDatabase', () => ({
  type: 'mysql',
  host: process.env.DB_HOST,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  logging: process.env.DB_LOGGING === 'true',
  synchronize: process.env.DB_SYNC === 'true',
  entities: [User],
  port: parseInt(process.env.DB_PORT, 10),
}));
